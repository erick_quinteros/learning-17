
##########################################################
#
#
# aktana- messageSequence estimates estimates Aktana Learning Engines.
#
# description: save messageSequence results
#
# created by : marc.cohen@aktana.com
# updated by : wendong.zhu@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016-2018.
#
#
####################################################################################################################

saveTimingScoreResult <- function(con, con_l, scores, BUILD_UID, RUN_UID, productUID, isNightly, runSettings, tteParams)
{
    library(data.table)
    library(futile.logger)
    library(ggplot2)
    library(scales)

    flog.info("entered saveTimingScoreResult function")    

    plotDir <- runSettings[["plotDir"]]
    
    sqlStr <- "SELECT repActionTypeId, repActionTypeName FROM RepActionType"
    repAction <- data.table(dbGetQuery(con, sqlStr))

    setnames(scores, "date", "predictionDate") 
    setnames(scores, "predict", "probability")
    
    segs <- tteParams[["segs"]]

    # remove segs not in AP
    accountProduct <- data.table(dbGetQuery(con, sprintf("SELECT * FROM AccountProduct limit 1")))
    segs <- segs[segs %in% colnames(accountProduct)]

    channelUID <- tteParams[["channelUID"]]
    channels <- tteParams[["channels"]]
    runStamp <- tteParams[["runStamp"]]

    nowDate <- Sys.Date()
    nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")

    
    if (isNightly) {
        # convert event type to id. 
        # Todo: id based on a list
        scores[event == 'S',  event := '12']
        scores[event == 'V', event := '3']
        scores[event == 'W', event := '13']
        scores[event == 'VS', event := '5']

        scores[, event := as.integer(event)]
        setnames(scores, "event", "repActionTypeId")  # rename event to repActionTypeId
        
        scores$runDate <- nowDate
        scores$createdAt <- nowTime
        scores$updatedAt <- nowTime

        tteAlgorithmName <- tteParams[["tteAlgorithmName"]]

        if (is.null(tteAlgorithmName)) {
          flog.info("tteAlgorithmName is NULL. Check whether the externalId/configUID of the build exists in TimeToEngageAlgorithm. Set to Manual Run.")
          tteAlgorithmName <- "TTE_Manual_Run"
        }

        scores$tteAlgorithmName <- tteAlgorithmName       

        if (channelUID == "SEND_CHANNEL") {  # optimize SEND channel
            if (!"VISIT" %in% channels) {  
                # listen to SEND channel only, so we only output best time to send email after SEND
                # for other channels, always output best time to send email afer VISIT or EVENT
                flog.info("listen to SEND channel in nightly run.")
                scores <- scores[repActionTypeId == 12] 
            }
        }
        
        # delete old data first.
        startTimer <- Sys.time()
        dbGetQuery(con, sprintf("DELETE FROM AccountTimeToEngage WHERE tteAlgorithmName = '%s'", tteAlgorithmName))

        flog.info("Delete old scores of AccountTimeToEngage: Time = %s", Sys.time()-startTimer)
        flog.info("Number of rows in scores = %s", nrow(scores))

        FIELDS <- list(accountId="int", repActionTypeId="tinyint", method="varchar(40)", tteAlgorithmName="varchar(80)", predictionDate="date", learningRunUID="varchar(80)", probability="double", runDate="date", createdAt="datetime", updatedAt="datetime")

        # append new scores/probs
        startTimer <- Sys.time()
        tryCatch(dbWriteTable(con, name="AccountTimeToEngage", value=as.data.frame(scores), overwrite=F, append=T, row.names=FALSE, field.types=FIELDS),
             error = function(e) {
               flog.error('Error in writing back to DSE table AccountTimeToEngage!', name='error')
               dbDisconnect(con)
               quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
             }
        )
        flog.info("Append new scores to DSE AccountTimeToEngage: Time = %s", Sys.time()-startTimer)            

        flog.info("Now plotting predictions for sample accounts ...")
        # sample accountIds and plot
        plotLoc <- sprintf("%s/Predictions_samples_SEND_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
        pdf(plotLoc)
        for (id in sample(scores$accountId, 300)) {
            print(ggplot(scores[accountId==id & repActionTypeId==12], aes(x=predictionDate, y=probability, color=probability))+geom_point()
            + labs(caption = sprintf("(accountId: %s)", id)) + scale_color_gradient(low="blue", high="red"))
        }
        dev.off()

        if (nrow(scores[repActionTypeId==3]) > 0) { # VISIT
            plotLoc <- sprintf("%s/Predictions_samples_VISIT_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
            pdf(plotLoc)
            for (id in sample(scores$accountId, 300)) {
                print(ggplot(scores[accountId==id & repActionTypeId==3], aes(x=predictionDate, y=probability, color=probability))+geom_point()
                + labs(caption = sprintf("(accountId: %s)", id)) + scale_color_gradient(low="blue", high="red"))
            }
            dev.off()
        }
        
        plotLoc <- sprintf("%s/Predictions_summary_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
        pdf(plotLoc)
        print(ggplot(scores[repActionTypeId==12], aes(x=probability)) + geom_histogram(aes(y = (..count..)/sum(..count..)), fill="blue", color="green" ) + 
            scale_y_continuous(labels = scales::percent) +  ylab("Percentage of Total") + xlab("Probability (SEND)"))
        print(ggplot(scores[repActionTypeId==3], aes(x=probability)) + geom_histogram(aes(y = (..count..)/sum(..count..)), fill="blue", color="green" ) +
            scale_y_continuous(labels = scales::percent) +  ylab("Percentage of Total") + xlab("Probability (VISIT)"))
        dev.off()

    } else { # manual job and score into Learning DB
      
        FIELDS <- list(accountUID="varchar(80)", channelUID="varchar(80)", method="varchar(40)", predictionDate="date", learningRunUID="varchar(80)", probability="double", createdAt="datetime", updatedAt="datetime")
        
        accountIdMap <- data.table(dbGetQuery(con,"SELECT accountId, externalId FROM Account WHERE isDeleted=0;"))

        # get accountUID from accountId
        scores <- merge(scores, accountIdMap, by="accountId")
        setnames(scores, "externalId", "accountUID")
        scores$accountId  <- NULL

        scores[event == "S", channelUID := "SEND"]
        scores[event == "V", channelUID := "VISIT"]
        scores$event <- NULL
        
        if (channelUID == "SEND_CHANNEL") {  # optimize SEND channel
            if (!"VISIT" %in% channels) {
                # listen to SEND channel only, so we only output best time to send email after SEND
                # for other channels, always output best time to send email afer VISIT or EVENT
                flog.info("listen to SEND channel in manual run.")
                scores <- scores[channelUID == "SEND"] 
            }
        }

        nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
        scores$createdAt <- nowTime
        scores$updatedAt <- nowTime  

        startTimer <- Sys.time()
        dbGetQuery(con_l, "TRUNCATE TABLE AccountTimeToEngage")

        flog.info("Delete old scores of AccountTimeToEngage in LearningDB: Time = %s", Sys.time()-startTimer)
        flog.info("Number of rows in scores = %s", nrow(scores))

        startTimer <- Sys.time()
        
        # Now write the new scores to database 
        tryCatch(dbWriteTable(con_l, name="AccountTimeToEngage", value=as.data.frame(scores), overwrite=F, append=T, row.names=FALSE, field.types=FIELDS),
             error = function(e) {
               flog.error('Error in writing back to table AccountTimeToEngage in learning DB!', name='error')
               dbDisconnect(con_l)
               quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
             }
        )  
        flog.info("Write scores to LearningDB AccountTimeToEngage Time = %s", Sys.time()-startTimer)  

        # sample accountUIDs and plot
        plotLoc <- sprintf("%s/Predictions_samples_SEND_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
        pdf(plotLoc)
        for (id in sample(scores$accountUID, 300)) {
            print(ggplot(scores[accountUID==id & channelUID=="SEND"], aes(x=predictionDate, y=probability, color=probability))+geom_point()
            + labs(caption = sprintf("(accountUID: %s)", id)) + scale_color_gradient(low="blue", high="red"))
        }
        dev.off()

        if (nrow(scores[channelUID=="VISIT"]) > 0) {
            plotLoc <- sprintf("%s/Predictions_samples_VISIT_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
            pdf(plotLoc)
            for (id in sample(scores$accountUID, 300)) {
                print(ggplot(scores[accountUID==id & channelUID=="VISIT"], aes(x=predictionDate, y=probability, color=probability))+geom_point()
                + labs(caption = sprintf("(accountUID: %s)", id)) + scale_color_gradient(low="blue", high="red"))
            }
            dev.off()
        }
        
        plotLoc <- sprintf("%s/Predictions_summary_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
        pdf(plotLoc)
        print(ggplot(scores[channelUID=="SEND"], aes(x=probability)) + geom_histogram(aes(y = (..count..)/sum(..count..)), fill="blue", color="green" ) + 
            scale_y_continuous(labels = scales::percent) +  ylab("Percentage of Total") + xlab("Probability (SEND)"))
        print(ggplot(scores[channelUID=="VISIT"], aes(x=probability)) + geom_histogram(aes(y = (..count..)/sum(..count..)), fill="blue", color="green" ) +
            scale_y_continuous(labels = scales::percent) +  ylab("Percentage of Total") + xlab("Probability (VISIT)"))
        dev.off()

    }

}
