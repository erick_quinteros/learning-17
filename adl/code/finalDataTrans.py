

from awsglue.transforms import *
from pyspark.sql.functions import *
from pyspark.sql.functions import mean, min, max
from pyspark.sql import Row, functions as F
from pyspark.sql.window import Window
from pyspark.sql.functions import when, lit, col

from pyspark.sql.types import IntegerType
from pyspark.sql.functions import col,expr,when
from utils import Utils
print('Import Successful')

class FinalDataTrans:
    def __init__(self, glue_context, customer, s3_destination, target):
        self.glueContext = glue_context
        self.customer = customer
        self.s3_destination = s3_destination
        self.utils = Utils(glue_context, customer)
        self.spark = self.glueContext.spark_session
        self.target = target
        pass

    def run(self):
        target=self.target
        sourcetable ="cri_scores"
        df_cri=self.utils.dataLoadDelta( self.s3_destination + "data/silver/" +  sourcetable)
        df_cri.cache()
        df_cri.createOrReplaceTempView("cri")
        print("cri cached")

        #read new recordclasses
        if target=='yes':
            sourcetable ="recordclasses_st"
            df_rc=self.utils.dataLoadDelta( self.s3_destination + "data/silver/" +  sourcetable)
            #df_rc=df_rc.withColumn('All_effectivedate',to_date(df_rc.All_effectivedate,'yyyy-MM-dd'))
            df_rc.cache()
            df_rc.createOrReplaceTempView("rc")
            print("rc cached")

        else:
            sourcetable ="recordclasses_dse"
            df_rc= self.utils.dataLoadDelta( self.s3_destination + "data/silver/" + sourcetable)
            #df_rc=df_rc.withColumn('All_effectivedate',to_date(df_rc.All_effectivedate,'yyyy-MM-dd'))
            df_rc.cache()
            df_rc.createOrReplaceTempView("rc")
            print("rc cached")
        df_rc=df_rc.withColumn("All_repId", df_rc["All_repId"].cast(IntegerType()))
        df_rc=df_rc.withColumn("All_accountId", df_rc["All_accountId"].cast(IntegerType()))

        sourcetable ="rep"
        df_rep= self.utils.dataLoadDelta( self.s3_destination + "data/bronze/" + sourcetable)
        df_rep.cache()
        df_rep.createOrReplaceTempView("rep")
        print("rep  cached")

        sourcetable ="rep_team_rep"
        df_repteamrep =self.utils.dataLoadDelta( self.s3_destination + "data/bronze/" + sourcetable)
        df_repteamrep.cache()
        df_repteamrep.createOrReplaceTempView("rtr")
        print("repteamrep  cached")


        sourcetable ="account"
        df_acct= self.utils.dataLoadDelta( self.s3_destination + "data/bronze/" + sourcetable)
        df_acct.cache()
        df_acct.createOrReplaceTempView("acct")
        print("account  cached")
        df_acct=df_acct.drop(df_acct.externalId)

        print(df_rep.columns)
        df_rc=df_rc.join(df_acct,(df_rc.All_accountId== df_acct.accountId),'left_outer').select(df_rc["*"],df_acct["*"])
        df_rc=df_rc.join(df_rep,(df_rc.All_repId== df_rep.repId),'left_outer').select(df_rc["*"],df_rep.repName,df_rep.rep_timeZoneId,df_rep.rep_isActivated,df_rep.rep_seConfigId,df_rep.rep_avgLatCurrentAccounts,df_rep.rep_avgLongCurrentAccounts,df_rep.avgMilesCurrentAccounts,df_rep.rep_maxMilesCurrentAccounts,df_rep.rep_numCurrAccounts,df_rep.rep_numCurrAccountsWithLatLong,df_rep.rep_numCurrAccountsWithValidLatLong)

        #scale CRI scores -----------------------------------------------------------------------------------------------------------------
        df_cri=df_cri.where( (~isnan(df_cri.Interaction_rep_account))   & (~isnan(df_cri.Email_open_score)) & (~isnan(df_cri.tenure_score)) &(~isnan(df_cri.visit_score)) &(~isnan(df_cri.cadence_score)) &(~isnan(df_cri.suggestion_visit_score)) &(~isnan(df_cri.suggestion_email_score)) &(~isnan(df_cri.target_achievement_score)) &(~isnan(df_cri.channel_score)) )

        w = Window.partitionBy("interaction_yearmonth")
        scaled_result = (col("tenure_score") - min("tenure_score").over(w)) / (max("tenure_score").over(w) - min("tenure_score").over(w))
        df_cri=df_cri.withColumn("tenure_score_std", F.round(scaled_result,2))

        scaled_result2 = (col("visit_score") - min("visit_score").over(w)) / (max("visit_score").over(w) - min("visit_score").over(w))
        df_cri=df_cri.withColumn("visit_score_std", F.round(scaled_result2,2))


        scaled_result3 = (col("cadence_score") - min("cadence_score").over(w)) / (max("cadence_score").over(w) - min("cadence_score").over(w))
        df_cri=df_cri.withColumn("cadence_score_std", 1-F.round(scaled_result3,2))

        scaled_result4 = (col("target_achievement_score") - min("target_achievement_score").over(w)) / (max("target_achievement_score").over(w) - min("target_achievement_score").over(w))
        df_cri=df_cri.withColumn("target_achievement_score_std", F.round(scaled_result4,2))

        df_cri=df_cri.select("Interaction_rep_account","interaction_yearmonth","Email_open_score","tenure_score_std","visit_score_std","cadence_score_std","suggestion_visit_score","suggestion_email_score","target_achievement_score_std","channel_score")

        df_cri=df_cri.withColumn("na_count",when(df_cri.Email_open_score.isNotNull(),1).otherwise(0)+when(df_cri.tenure_score_std.isNotNull(),1).otherwise(0)+when(df_cri.visit_score_std.isNotNull(),1).otherwise(0)+when(df_cri.cadence_score_std.isNotNull(),1).otherwise(0)+when(df_cri.suggestion_visit_score.isNotNull(),1).otherwise(0)+when(df_cri.suggestion_email_score.isNotNull(),1).otherwise(0)+when(df_cri.target_achievement_score_std.isNotNull(),1).otherwise(0)+when(df_cri.channel_score.isNotNull(),1).otherwise(0) )

        df_cri=df_cri.withColumn("sum_index",F.round(when(df_cri.Email_open_score.isNull(),0).otherwise(df_cri.Email_open_score)+when(df_cri.tenure_score_std.isNull(),0).otherwise(df_cri.tenure_score_std)+when(df_cri.visit_score_std.isNull(),0).otherwise(df_cri.visit_score_std)+when(df_cri.cadence_score_std.isNull(),0).otherwise(df_cri.cadence_score_std)+when(df_cri.suggestion_visit_score.isNull(),0).otherwise(df_cri.suggestion_visit_score)+when(df_cri.suggestion_email_score.isNull(),0).otherwise(df_cri.suggestion_email_score)+when(df_cri.target_achievement_score_std.isNull(),0).otherwise(df_cri.target_achievement_score_std)+when(df_cri.channel_score.isNull(),0).otherwise(df_cri.channel_score),2) )
        df_cri=df_cri.withColumn("index",when(df_cri.na_count>2,F.round(df_cri.sum_index/df_cri.na_count,2)).otherwise(lit(None)))

        df_cri=df_cri.select("Interaction_rep_account","interaction_yearmonth","Email_open_score","tenure_score_std","visit_score_std","cadence_score_std","suggestion_visit_score","suggestion_email_score","target_achievement_score_std","channel_score","sum_index","index")

        df_cri.show()





        df_final=df_rc.join(df_cri,(df_rc.All_rep_account== df_cri.Interaction_rep_account) & (df_rc.All_effectiveyearmonth== df_cri.interaction_yearmonth),'left_outer').select(df_rc["*"],df_cri.Email_open_score,df_cri.tenure_score_std,df_cri.visit_score_std,df_cri.cadence_score_std,df_cri.suggestion_visit_score,df_cri.suggestion_email_score,df_cri.target_achievement_score_std,df_cri.channel_score,df_cri.sum_index,df_cri.index).dropDuplicates()

        x = df_final.select("Email_Email_Subject__c").distinct()
        x = x.withColumn("Email_Subject_Id", monotonically_increasing_id())
        df_final = df_final.join(x, (df_final.Email_Email_Subject__c == x.Email_Email_Subject__c), 'left_outer').select(
            df_final["*"], x["Email_Subject_Id"])
        df_final=df_final.withColumn("recordclasses_id",df_final["recordclassId"])

        df_final.repartition("recordclassId").write.mode('overwrite').partitionBy("recordclassId").format("delta").save(self.s3_destination+"data/silver/final_dataset/")