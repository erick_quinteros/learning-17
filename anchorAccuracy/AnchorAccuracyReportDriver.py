import sys
import os

# Update sys path to find the modules in common folder
script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
learning_dir = os.path.dirname(script_dir)
sys.path.append(learning_dir)

from common.pyUtils.logger import get_module_logger
from common.pyUtils.database_utils import DatabasePoolConnection, DatabasePoolOpContextManager, DatabaseOpContextManager, other_database_op
from common.pyUtils.database_config import DatabaseConfig
from common.pyUtils.database_connection import connect_to_db_all

from anchorAccuracy.NightlyAccuracyReporter import NightlyAccuracyReporter
from anchorAccuracy.HistoryAccuracyReporter import HistoryAccuracyReporter
from anchorAccuracy.utils.AnchorTableNamesConfig import AnchorTableNamesConfig

logger = get_module_logger(__name__)


class AnchorAccuracyReportDriver:
    """
    driver class to connect objects to run anchor accuracy report
    """

    def __init__(self, input_arg_dicts):

        # initialize db connnection config
        DatabaseConfig.instance().set_config(**input_arg_dicts)

        # process input
        self.rundate = input_arg_dicts['rundate']
        self.homedir = input_arg_dicts['homedir']
        self.envname = input_arg_dicts['envname']
        self.customer = input_arg_dicts['customer']
        self.runStartDate = input_arg_dicts['runStartDate'] if 'runStartDate' in input_arg_dicts else None
        self.runEndDate = input_arg_dicts['runEndDate'] if 'runEndDate' in input_arg_dicts else None
        self.is_nightly = True if self.runStartDate is None or self.runEndDate is None else False
        self.conn_pool = None

        # update input (anchor result) & output (accuracy result) tables config
        self.paraRun_model = input_arg_dicts['runmodel'] if 'runmodel' in input_arg_dicts else None
        AnchorTableNamesConfig.instance().set_config(self.paraRun_model)

        # log instance properties
        logger.info("Initialized AnchorAccuracyReportDriver instance with {}".format(", ".join("{}={}".format(k, v) for k, v in self.__dict__.items())))

    def connect_to_db(self):
        self.conn_pool = connect_to_db_all(max_overflow=5, pool_size=1)

    def close_db_conn(self):
        self.conn_pool.close_all_conn()

    def run(self):
        try:
            self.connect_to_db()
            if self.is_nightly:
                NightlyAccuracyReporter(self.conn_pool).run()
            else:
                HistoryAccuracyReporter(self.conn_pool, self.runStartDate, self.runEndDate).run()
            self.close_db_conn()
        except Exception as e:
            logger.error("Running AnchorAccuracyReportDriver failed:{}".format(e))
            raise


def main():
    """
    function to run Driver class
    :return:
    """
    input_args_dict = dict(i.split("=") for i in sys.argv[1:])
    AnchorAccuracyReportDriver(input_args_dict).run()


if __name__ == "__main__":
    main()