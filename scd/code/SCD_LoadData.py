# Databricks notebook source
import pandas as pd

#set spark configuration to enable pyarrow in order to spead up conversion of panda dataframe
spark.conf.set("spark.sql.execution.arrow.enabled", "true")
#spark.conf.set("spark.rpc.message.maxSize","1024")
# use the secrets API to set these values
user = dbutils.secrets.get("aktana", "username")
password = dbutils.secrets.get("aktana", "password")

options = {
  "sfUrl": "aktanapartner.snowflakecomputing.com/", 
  "sfUser": user,
  "sfPassword": password,
  "sfDatabase": "RPT_DWPREPROD",
  "sfSchema": "DW_CENTRAL",
  "sfWarehouse": "AKTANADEV_WH_US_WEST_2",
}

# Read the data written by the previous cell back.
VW_BRICK_WEEKLY_SALES_VOLUME= spark.read \
  .format("snowflake") \
  .options(**options) \
  .option("dbtable", "VW_BRICK_WEEKLY_SALES_VOLUME") \
  .load()
VW_BRICK_WEEKLY_MARKETBASKET_VOLUME = spark.read \
  .format("snowflake") \
  .options(**options) \
  .option("dbtable", "VW_BRICK_WEEKLY_MARKETBASKET_VOLUME") \
  .load()
VW_BRICK_WEEKLY_ORDER_GAP_DETECTION = spark.read \
  .format("snowflake") \
  .options(**options) \
  .option("dbtable", "VW_BRICK_WEEKLY_ORDER_GAP_DETECTION") \
  .load()
VW_BRICK_WEEKLY_MARKET_SHARE = spark.read \
  .format("snowflake") \
  .options(**options) \
  .option("dbtable", "VW_BRICK_WEEKLY_MARKET_SHARE") \
  .load()
VW_BRICK_WEEKLY_SALES_VOLUME_PDF=VW_BRICK_WEEKLY_SALES_VOLUME.toPandas()
VW_BRICK_WEEKLY_MARKETBASKET_VOLUME_PDF= VW_BRICK_WEEKLY_MARKETBASKET_VOLUME.toPandas()
VW_BRICK_WEEKLY_ORDER_GAP_DETECTION_PDF = VW_BRICK_WEEKLY_ORDER_GAP_DETECTION.toPandas()
VW_BRICK_WEEKLY_MARKET_SHARE_PDF = VW_BRICK_WEEKLY_MARKET_SHARE.toPandas()

#display(VW_BRICK_WEEKLY_SALES_VOLUME_PDF)