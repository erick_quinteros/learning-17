import os
import sys
import logging
import datetime

# Update sys path to find the modules from Common and DataAccessLayer
script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
learning_dir = os.path.dirname(script_dir)
sys.path.append(learning_dir)

from pyspark.sql import SparkSession
from pyspark.sql import SQLContext
from pyspark.sql.functions import year, month, udf, countDistinct, sum, col, trim, min
from pyspark import SparkContext
from datetime import datetime, timedelta
from dateutil.relativedelta import *
from common.pyUtils.logger import get_module_logger
from common.DataAccessLayer.SparkDataAccessLayer import initialize, CommonDbAccessor, SparkDSEAccessor, SparkLearningAccessor, SparkCSAccessor, SparkStageAccessor
from common.DataAccessLayer.DataAccessLayer import connect_learning_database, connect_dse_database
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from common.logger.Logger import create_logger


logger = get_module_logger("REP_ENGAGEMENT_CALCULATION")


# Global variable
spark = None
numPartitions = 64


class RepEngagementCalculator:
    """
    This class is responsible for computing rep engagement probability
    """

    def __init__(self, learning_home_dir, database_config):
        logger.info("Initializing")
        self.learning_home_dir = learning_home_dir
        self.database_config = database_config

        self.dse_db_name = database_config.dse_db_name
        self.db_learning = database_config.learning_db_name

        self.dse_accessor = SparkDSEAccessor()
        self.learning_accessor = SparkLearningAccessor()
        self.cs_accessor = SparkCSAccessor()
        self.stage_accessor = SparkStageAccessor()

    def format_dates(self, today, date_past):
        date_past_month = date_past.strftime("%m")
        date_past_year = date_past.strftime("%Y")
        today_format = today.strftime("%Y-%m-%d")
        date_past_format = "{past_year}-{past_month}-01".format(past_year=date_past_year, past_month=date_past_month)
        start_date_string = " where startDateLocal >= '{START}'".format(START=date_past_format)
        end_date_string = " and startDateLocal <= '{END}'".format(END=today_format)
        published_suggestions_condition = "WHERE DR.startDateLocal BETWEEN '{START} 00:00:00' AND '{END} 23:59:59' ".format(START=date_past_format, END=today_format)
        synched_Suggestions_condition = "AND A.Sync_Completed_Datetime_vod__c > '{START} 00:00:00'".format(START=date_past_format)
        logger.debug("Start date={START_DATE} and end date={END_DATE} to run rep engagement".format(START_DATE=date_past_format, END_DATE=today_format))
        return start_date_string, end_date_string, published_suggestions_condition, synched_Suggestions_condition

    def get_initial_tables(self, published_suggestions_condition):
        #Gets DSE Run, DSE Run Rep Date, Spark DSE Run, and Spark DSE Run Rep Date tables
        dse_run_df = self.dse_accessor.get_run_df(published_suggestions_condition)
        logger.info("Success fetching DSE RUN Table")
        dseRunRepDate_df = self.dse_accessor.get_run_rep_date_df()
        logger.info("Success fetching DSERunRepDate Table")
        spark_dse_run_df = self.dse_accessor.get_spark_run_df(published_suggestions_condition)
        logger.info("Success fetching Spark DSE RUN Table")
        spark_dse_run_rep_Date_df = self.dse_accessor.get_spark_run_rep_date_df()
        logger.info("Success fetching SparkDSERunRepDate Table")
        return dse_run_df, dseRunRepDate_df, spark_dse_run_df, spark_dse_run_rep_Date_df

    def join_DSErun_DSErunrepdate(self, dserun_df, dseRunRepDate_df):
        logger.info("Started joining DSERun and DSERunRepDate")
        publishedDate = udf(lambda startDateTime: startDateTime.strftime("%Y-%m-%d %H:%M:%S") if (startDateTime) else None)
        nextPublishedDate = udf(lambda startDateTime_old, startDateTime_new: startDateTime_new.strftime("%Y-%m-%d %H:%M:%S") if (startDateTime_old < startDateTime_new) else None)
        final_df_one = dserun_df.alias("a").join(dseRunRepDate_df.alias("b"), on=['runId']).groupBy("a.runId", "a.startDateLocal", "b.repId", "a.startDateTime")\
            .agg(publishedDate("a.startDateTime").alias("publishedDate"))
        logger.info("Success joining DSERun and DSERunRepDate")
        return final_df_one, nextPublishedDate

    def calculate_next_published_date(self, final_df_one, is_spark, nextPublishedDate):
        #Determines next published date based on the state and publish dates
        logger.info("Calculating next Published date")
        final_df_two = final_df_one
        cond = [final_df_one.repId == final_df_two.repId]
        if is_spark:
            nextPublished = final_df_one.alias("a").join(final_df_two.alias("b"), cond, how="left").groupBy("a.repId", "a.runUID", "a.publishedDate", "a.startDateLocal").agg(min(nextPublishedDate("a.startDateTime", "b.startDateTime")).alias("nextPublishedDate"))
            nextPublished = nextPublished.withColumnRenamed("runUID", "runId")
        else:
            nextPublished = final_df_one.alias("a").join(final_df_two.alias("b"), cond, how="left").groupBy("a.repId", "a.runId", "a.publishedDate", "a.startDateLocal").agg(min(nextPublishedDate("a.startDateTime", "b.startDateTime")).alias("nextPublishedDate"))
        logger.info("Success next Published date")
        return nextPublished

    def join_spark_DSErun_DSErunrepdate(self, dserun_df, dseRunRepDate_df):
        logger.info("Started joining SparkDSERun and SparkDSERunRepDate")
        publishedDate = udf(lambda startDateTime: startDateTime.strftime("%Y-%m-%d %H:%M:%S") if (startDateTime) else None)
        nextPublishedDate = udf(lambda startDateTime_old, startDateTime_new: startDateTime_new.strftime("%Y-%m-%d %H:%M:%S") if (startDateTime_old < startDateTime_new) else None)
        final_df_one = dserun_df.alias("a").join(dseRunRepDate_df.alias("b"), on=['runUID']).groupBy("a.runUID", "b.repId", "a.startDateLocal", "a.startDateTime")\
            .agg(publishedDate("a.startDateTime").alias("publishedDate"))
        logger.info("Success joining SparkDSERun and SparkDSERunRepDate")
        return final_df_one, nextPublishedDate

    def merge_published_dates(self, nextPublished, nextPublished_spark):
        logger.info("Merging both spark and non-spark next published dates")
        nextPublished_total = nextPublished.union(nextPublished_spark)
        logger.info("Success merging both spark and non-spark next published dates")
        return nextPublished_total

    def get_rep_table(self):
        logger.info("Getting Rep Table")
        rep_df = self.dse_accessor.get_rep_df()
        logger.info("Success fetching Rep Table")
        return rep_df

    def merge_next_published_with_rep(self, next_published_tot, rep_df):
        logger.info("Merging next published dates with REP table")
        next_published_tot = next_published_tot.alias("a").join(rep_df.alias("b"), on="repId", how="left").select("a.repId", "a.runId", "a.publishedDate", "a.nextPublishedDate", "b.externalId", "a.startDateLocal")
        logger.info("Success merging next published dates with REP table")
        return next_published_tot

    def get_sync_info(self, synched_Suggestions_condition):
        logger.info("Getting sync info Table")
        sync_tracking_df = self.cs_accessor.get_sync_info_table(synched_Suggestions_condition)
        logger.info("Success fetching sync info Table")
        return sync_tracking_df

    def merge_sync_info_with_externalID(self, next_published_tot, sync_tracking_df):
        logger.info("Merging sync info and externalID")
        externalId_df = next_published_tot.select('externalId').distinct()
        cond = [sync_tracking_df.OwnerId == externalId_df.externalId]
        sync_info_df = sync_tracking_df.alias("a").join(externalId_df.alias("b"), cond, how="left").select("a.OwnerId", "a.CreatedDate")
        sync_info_df = sync_info_df.withColumnRenamed("OwnerId", "externalId")
        sync_info_df = sync_info_df.withColumnRenamed("CreatedDate", "syncStart")
        logger.info("Success merging sync info and externalID")
        return sync_info_df

    def merge_published_suggestions_with_syncs(self, next_published_tot, sync_info_df):
        logger.info("Merging Published suggestions and syncs")
        column_in_list = udf(lambda PublishDateTime, NextPublishDateTime, SyncStart: 1 if (
                (SyncStart.strftime("%Y-%m-%d %H:%M:%S") >= PublishDateTime if SyncStart else 0) and (
            SyncStart.strftime("%Y-%m-%d %H:%M:%S") <= NextPublishDateTime if NextPublishDateTime else 1)) else 0)
        result_df = next_published_tot.alias("a").join(sync_info_df.alias("b"), on="externalId", how="left").groupby("a.repId", "a.startDateLocal", "a.runId") \
            .agg(sum(column_in_list("a.publishedDate", "a.nextPublishedDate", "b.syncStart")).alias("Times_Synched"))
        synched_runs_df = result_df.withColumnRenamed("startDateLocal", "StartDateLocalSynched")
        logger.info("Success Merging Published suggestions and syncs")
        return synched_runs_df


    def initialize_df(self):
        #Connects to db, selects DSE data, and merges df to prepare for calculations

        logger.info("Started RepEngagement Calculation")
        today = datetime(2020, 1, 1, 12, 00, 00)
        date_past = today + relativedelta(years=-2)
        start_date_string, end_date_string, published_suggestions_condition, synched_Suggestions_condition = self.format_dates(today, date_past)
        repEngagement_df = self.learning_accessor.get_rep_engagement_table()
        if repEngagement_df.count() > 0:
            logger.info("Engagement Calculation on last 2 months of data")
            date_past = today + relativedelta(months=-2)
            start_date_string, end_date_string, published_suggestions_condition, synched_Suggestions_condition = self.format_dates(today, date_past)
        dse_run_df, dseRunRepDate_df, spark_dse_run_df, spark_dseRunRepDate_df = self.get_initial_tables(published_suggestions_condition)
        final_df, nextPublishedDate = self.join_DSErun_DSErunrepdate(dse_run_df, dseRunRepDate_df)
        next_published = self.calculate_next_published_date(final_df, 0, nextPublishedDate)
        spark_final_df, nextPublishedDate = self.join_spark_DSErun_DSErunrepdate(spark_dse_run_df, spark_dseRunRepDate_df)
        spark_next_published = self.calculate_next_published_date(spark_final_df, 1, nextPublishedDate)
        next_published_tot = self.merge_published_dates(next_published, spark_next_published)
        rep_df = self.get_rep_table()
        next_published_tot = self.merge_next_published_with_rep(next_published_tot, rep_df)
        sync_tracking_df = self.get_sync_info(synched_Suggestions_condition)
        sync_info_df = self.merge_sync_info_with_externalID(next_published_tot, sync_tracking_df)
        synched_runs_df = self.merge_published_suggestions_with_syncs(next_published_tot, sync_info_df)
        return today, start_date_string, end_date_string, repEngagement_df, synched_runs_df

    def get_AKT_RepLicense_arc(self):
        logger.info("Getting AKT_RepLicense_arc")
        rep_license_arc_df = self.stage_accessor.get_AKT_rep_license_arc()
        return rep_license_arc_df

    def get_rpt_dim_calendar(self):
        logger.info("Getting rpt_dim_calendar")
        dim_calendar_df = self.stage_accessor.get_rpt_dim_calendar()
        return dim_calendar_df

    def get_rpt_Suggestion_Delivered_stg(self, start_date_string, end_date_string, synched_runs_df):
        logger.info("Getting RPT_Suggestion_Delivered_stg")
        suggestion_Delivered_df_raw = self.stage_accessor.get_rpt_Suggestion_Delivered_stg(start_date_string, end_date_string)
        logger.info("Success fetching RPT_Suggestion_Delivered_stg")
        suggestion_Delivered_df_raw.cache()
        logger.info("Start eliminating non-synched runIds per rep")
        suggestion_Delivered_df = suggestion_Delivered_df_raw.alias("a").join(synched_runs_df.alias("b"),
                                                                          on=['repId', 'runId'], how='left')
        suggestion_Delivered_df = suggestion_Delivered_df.fillna({'Times_Synched': 0})
        suggestion_Delivered_df = suggestion_Delivered_df.where(suggestion_Delivered_df.Times_Synched > 0)
        logger.info("Success eliminating non-synched runIds per rep")
        suggestion_Delivered_df_raw.unpersist()
        return suggestion_Delivered_df

    def merge_suggestions_delivered_with_AKT_RepLicense_arc(self, suggestion_Delivered_df, repLicenseTable_df):
        logger.info("Merging Suggestion delivered stage table & AKT_RepLicense_arc table")
        cond = [suggestion_Delivered_df.repUID == repLicenseTable_df.externalId,
                suggestion_Delivered_df.startDateLocal >= repLicenseTable_df.startDate,
                suggestion_Delivered_df.startDateLocal <= repLicenseTable_df.endDate]
        SuggDel_RepLicence_df = suggestion_Delivered_df.alias("a").join(repLicenseTable_df.alias("b"), cond).select(
            col("a.startDateLocal"), col("a.repUID"), col("a.repName"), col("b.cluster"), col("a.seConfigId"),
            col("a.seConfigName"), col("a.suggestionDriver"), col("a.suggestionReferenceId"), col("b.territoryId"),
            col("b.territoryName"), col("a.suggestedDate"), col("a.actionTaken"))
        logger.info("Merge Success Suggestion delivered stage table & AKT_RepLicense_arc table")
        return SuggDel_RepLicence_df

    def merge_suggestions_delivered_RepLicense_with_rpt_dim(self, SuggDel_RepLicence_df, dim_calendar_df):
        logger.info("Merging Suggestion_delivered_RepLicence & rpt_dim_calendar table")
        cond = [SuggDel_RepLicence_df.startDateLocal == dim_calendar_df.datefield]
        suggestions_df = SuggDel_RepLicence_df.join(dim_calendar_df, cond, how='left')
        logger.info("Merge Success Suggestion_delivered_RepLicence & rpt_dim_calendar table")
        return suggestions_df

    def filter_holidays(self, suggestions_df):
        #Removes actions not taken on holidays and weekends
        logger.info("Filtering out holidays, weekends table and action not taken")
        suggestions_df = suggestions_df[(suggestions_df['is_holiday'] != 1) | (suggestions_df['is_weekend'] != 1)]
        logger.info("Success Filtering out holidays, weekends table and action not taken")
        return suggestions_df

    def add_colums_df(self, suggestions_df):
        #Adds additional columns to prepare for calculations
        logger.info("Adding Columns to DataFrame")
        suggestions_df = suggestions_df.withColumn("year", year(suggestions_df.startDateLocal))
        suggestions_df = suggestions_df.withColumn("month", month(suggestions_df.startDateLocal))
        udf_cluster_upper = udf(lambda x: x.upper())
        suggestions_df = suggestions_df.withColumn("repTeamUID", udf_cluster_upper('cluster'))
        suggestions_df = suggestions_df.withColumn("repTeamName", udf_cluster_upper('cluster'))
        suggestions_df = suggestions_df.withColumn("suggestionType", suggestions_df.suggestionDriver)
        suggestions_df_Delivered = suggestions_df.groupBy("suggestionReferenceId", "year", "month", "seConfigId",
                                                          "suggestionType", "territoryId").agg(
            countDistinct("suggestedDate").alias("suggestionsDelivered"))
        suggestions_df = suggestions_df.alias("a").join(suggestions_df_Delivered.alias("b"), 'suggestionReferenceId',
                                                        how='left') \
            .select("a.year", "a.month", "a.repUID", "a.repName", "a.seConfigId", "a.seConfigName", "a.repTeamUID",
                    "a.repTeamName", "a.suggestionType", "a.suggestionReferenceId", "a.territoryId", "a.territoryName",
                    "b.suggestionsDelivered", "a.actionTaken")
        logger.info("Success adding Columns to DataFrame")
        return suggestions_df, udf_cluster_upper

    def suggestions_delivered_calculation(self, suggestions_df):
        #Isolates delivered suggestions in seperate df
        logger.info("Starting Suggestions Delivered Calculation")
        total_suggestions_delivered_count = suggestions_df.groupBy("year", "month", "repUID", "seConfigId",
                                                                   "suggestionType", "territoryId").agg(
            sum('suggestionsDelivered').alias('totalSuggestionsDeliveredTimes'))
        suggestions_delivered_df = suggestions_df.alias("a").join(total_suggestions_delivered_count.alias("b"),
                                                                  on=['repUID', 'year', 'month', 'seConfigId',
                                                                      'suggestionType', 'territoryId']) \
            .select("a.year", "a.month", "a.repUID", "a.repName", "a.seConfigId", "a.seConfigName", "a.suggestionType",
                    "a.territoryId", "a.territoryName", "b.totalSuggestionsDeliveredTimes")
        suggestions_delivered_df = suggestions_delivered_df.drop_duplicates()
        logger.info("Success Calculating Suggestions Delivered")
        return suggestions_delivered_df

    def engaged_suggestions_calculations(self, suggestions_df):
        #Isolates engaged suggestions in seperate df
        logger.info("Starting Engaged Suggestions Calculation")
        suggestions_df = suggestions_df[(suggestions_df['actionTaken'] != 'No Action Taken')]
        engaged_suggestions_count = suggestions_df.groupBy("year", "month", "repUID", "seConfigId", "suggestionType",
                                                           "territoryId").agg(
            sum('suggestionsDelivered').alias('engagedUniqueSuggestionsCount'))
        engaged_suggestions_df = suggestions_df.alias("a").join(engaged_suggestions_count.alias("b"),
                                                                on=['repUID', 'year', 'month', 'seConfigId',
                                                                    'suggestionType', 'territoryId']) \
            .select("a.year", "a.month", "a.repUID", "a.repName", "a.seConfigId", "a.seConfigName", "a.suggestionType",
                    "a.territoryId", "a.territoryName", "b.engagedUniqueSuggestionsCount")
        engaged_suggestions_df = engaged_suggestions_df.drop_duplicates()
        logger.info("Success Engaged Suggestions Delivered")
        return engaged_suggestions_df

    def merge_suggestions_delivered_with_engaged(self, udf_cluster_upper, suggestions_delivered_df, engaged_suggestions_df, repLicenseTable_df):
        #Merge suggestions with engaged, and finalize df
        logger.info("Merging Suggestion delivered & Engaged suggestions info")
        df = suggestions_delivered_df.alias("a").join(engaged_suggestions_df.alias("b"),
                                                      on=['repUID', 'year', 'month', 'seConfigId', 'suggestionType',
                                                          'territoryId'], how="left") \
            .select("a.year", "a.month", "a.repUID", "a.repName", "a.seConfigId", "a.seConfigName", "a.suggestionType",
                    "a.territoryId", "a.territoryName", "a.totalSuggestionsDeliveredTimes",
                    "b.engagedUniqueSuggestionsCount")
        df = df.fillna({'engagedUniqueSuggestionsCount': 0})
        repLicenseTable_df = repLicenseTable_df.withColumn("repTeamUID", udf_cluster_upper('cluster'))
        repLicenseTable_df = repLicenseTable_df.withColumn("repTeamName", udf_cluster_upper('cluster'))
        repLicenseTable_df = repLicenseTable_df.withColumnRenamed("externalId", "repUID")
        final_df = df.alias("a").join(repLicenseTable_df.alias("b"), on="repUID", how='left') \
            .select("a.year", "a.month", "a.repUID", "a.repName", "a.seConfigId", "a.seConfigName", "b.repTeamUID",
                    "b.repTeamName", "a.suggestionType", "a.territoryId", "a.territoryName",
                    "a.totalSuggestionsDeliveredTimes", "a.engagedUniqueSuggestionsCount")
        final_df = final_df.drop_duplicates()
        logger.info("Finished merging Suggestion delivered & Engaged suggestions info")
        return final_df

    def finalize_df(self, start_date_string, end_date_string, synched_runs_df):
        #Determine suggestions delivered, accepted, and not acted on to calculate engagement

        repLicenseTable_df = self.get_AKT_RepLicense_arc()
        dim_calendar_df = self.get_rpt_dim_calendar()
        suggestion_Delivered_df = self.get_rpt_Suggestion_Delivered_stg(start_date_string, end_date_string, synched_runs_df)
        suggestion_Delivered_df.cache()
        SuggDel_RepLicence_df = self.merge_suggestions_delivered_with_AKT_RepLicense_arc(suggestion_Delivered_df, repLicenseTable_df)
        suggestions_df = self.merge_suggestions_delivered_RepLicense_with_rpt_dim(SuggDel_RepLicence_df, dim_calendar_df)
        suggestions_df = self.filter_holidays(suggestions_df)
        suggestions_df, udf_cluster_upper = self.add_colums_df(suggestions_df)
        suggestions_delivered_df = self.suggestions_delivered_calculation(suggestions_df)
        engaged_suggestions_df = self.engaged_suggestions_calculations(suggestions_df)

        final_df = self.merge_suggestions_delivered_with_engaged(udf_cluster_upper, suggestions_delivered_df, engaged_suggestions_df, repLicenseTable_df)
        suggestion_Delivered_df.unpersist()
        return final_df

    def truncating_data(self, repEngagement_df, today):
        logger.info("Started truncating last 2-3 months of data")
        if repEngagement_df.count() > 0:
            connection = connect_learning_database()
            cursor = connection.cursor(buffered=True)
            for i in range(3):
                date_past = today + relativedelta(months=-i)
                month = date_past.strftime("%m")
                year = date_past.strftime("%Y")
                logger.debug("Deleting {MONTH}, {YEAR}".format(MONTH=month, YEAR=year))
                sql_query = "DELETE FROM {DB_NAME}.RepEngagementCalculation WHERE month = '{MONTH}' AND year = '{YEAR}'".format(DB_NAME=self.db_learning, MONTH=month, YEAR=year)
                cursor.execute(sql_query)
                # print("CURSOR __________________________________")
                print(sql_query)
            connection.commit()
            cursor.close()
            connection.close()
        logger.info("Truncating Done")

    def populating_RepEngagementTable(self, final_df):
        #Populates db with truncated data
        logger.info("Now populating rep engagement calculations to RepEngagementTable")
        self.learning_accessor.write_rep_engagement_table(final_df)
        logger.info("DB Populating complete")

    def populate_db(self, today, repEngagement_df, final_df):
        #Truncates and populates db

        logger.info("Rep Engagement Calculation complete")
        self.truncating_data(repEngagement_df, today)
        self.populating_RepEngagementTable(final_df)

    def update_ChannelActionMap(self):

        logger.info("Now updating ChannelActionMap")

        connection = connect_dse_database()

        cursor = connection.cursor(buffered=True)

        sql_query_template = "UPDATE {tableName_channelActionMap}" \
                             " SET appearsInInteractionData = 1" \
                             " WHERE repActionTypeId IN (SELECT DISTINCT repActionTypeId FROM {tableName_interaction})"


        tableName_channelActionMap = "{db_learning}.ChannelActionMap".format(db_learning=self.db_learning)
        tableName_interaction = "{db_dse}.Interaction".format(db_dse=self.dse_db_name)

        sql_query = sql_query_template.format(tableName_channelActionMap=tableName_channelActionMap,
                                              tableName_interaction=tableName_interaction)

        logger.info("SQL to update ChannelActionMap: {sql_query}".format(sql_query=sql_query))

        cursor.execute(sql_query)
        connection.commit()
        cursor.close()
        connection.close()
        logger.info("ChannelActionMap has been updated")

    def calculate(self):
        """
            This function gives engagement level for all reps, territories,
            suggestionType, configs, as well as update the Channel Action Map
            :return:
        """
        today, start_date_string, end_date_string, repEngagement_df, synched_runs_df = self.initialize_df()
        final_df = self.finalize_df(start_date_string, end_date_string, synched_runs_df)
        self.populate_db(today, repEngagement_df, final_df)
        self.update_ChannelActionMap()

def main():
    global spark
    global numPartitions

    # Validate the input argument to the script
    if len(sys.argv) != 12:
        logger.error("Invalid arguments to the optimizer script")
        exit(0)

    # Retrieve the arguments
    db_host = sys.argv[1]
    db_user = sys.argv[2]
    db_password = sys.argv[3]
    dse_db_name = sys.argv[4]
    cs_db_name = sys.argv[5]
    stage_db_name sys.argv[6]
    learning_db_name = sys.argv[7]
    db_port = sys.argv[8]  # Update to 33066 for testing on Local machine
    learning_home_dir = sys.argv[9]
    customer = sys.argv[10]
    environment = sys.argv[11]

    # Get the singleton instance of the database config and set the properties

    database_config = DatabaseConfig.instance()
    database_config.set_config(db_host, db_user, db_password, db_port, dse_db_name, learning_db_name, cs_db_name, stage_db_name)

    # Create spark context
    spark = SparkSession.builder.appName("RepEngagementCalculators").getOrCreate()

    initialize("RepEngagementCalculatorLogger", spark)
    RepEngagementCalculator(learning_home_dir, database_config).calculate()

if __name__ == "__main__":
    main()
