import os
import sys

# Update sys path to find the modules from Common and DataAccessLayer
script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
learning_dir = os.path.dirname(script_dir)
sys.path.append(learning_dir)
#import packages
from common.DataAccessLayer.DataAccessLayer import MessageAccessor
from common.DataAccessLayer.DataAccessLayer import SegmentAccessor
from common.DataAccessLayer.DataAccessLayer import DatabaseIntializer
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from common.DataAccessLayer.DataAccessLayer import LearningAccessor
from common.DataAccessLayer.DataAccessLayer import DSEAccessor
import itertools
import datetime
from common.logger.Logger import create_logger
import common.DataAccessLayer.DataAccessLayer as data_access_layer
import logging
import pandas as pd
import time
import requests
import json
# import threading

logger = None
TOKEN_URL = 'https://www.arcgis.com/sharing/oauth2/token'
BASE_URL = 'http://geocode.arcgis.com/'
FIND_SUFFIX = 'arcgis/rest/services/World/GeocodeServer/geoCodeAddresses'
FIND_URL = BASE_URL + FIND_SUFFIX
EXPIRATION = 180  # expiration time in minutes
BATCH_SIZE = 1000

class GeocodeGeneratorDriver:
    def __init__(self, learning_home_dir, customer_name):
        self.learning_home_dir = learning_home_dir
        self.__initialize_logger()
        self.customer = customer_name
        self.targetCountry = self.customer[-2:].upper()

    def __initialize_logger(self):
        """

        :return:
        """
        # Create logger path
        timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        log_file_path = self.learning_home_dir + "/../logs/geocodeGeneratorDriver." + timestamp + ".stdout"

        # Crete logger with file and output
        global logger
        logger_name = "geocodeGeneratorDriver"
        logger = create_logger(logger_name, log_file_path, logging.DEBUG)

        # Initialize data access layer logger
        data_access_layer.initialize_logger(logger_name)

    def startGeocodeFacility(self):
        logger.info("geocode generator start...")
        starttime = time.time()
        
        # get facility dataframe
        dse_accessor = DSEAccessor()
        facilities = dse_accessor.get_facility_df()
        facilities['Address'] = facilities['facilityName'] + " " + facilities['geoLocationString']
        logger.info("Get {} facilities from DB".format(facilities.shape[0]))

        # handle abbr
        facilities = self.handleAbbr(facilities)

        # find distinct facilities
        facilities_distinct = facilities.drop_duplicates('Address')
        n_row, n_col = facilities_distinct.shape
        logger.info("Number of distinct facilities: {}".format(n_row))

        # prepare api params
        params = dict()
        params['f'] = 'pjson'
        # params['forStorage'] = False
        params['sourceCountry'] = self.targetCountry
        learning_accessor = LearningAccessor()
        client_id, client_secret = learning_accessor.get_arcgis_credential()
        token = self.getAPIToken(client_id=client_id, client_secret=client_secret)
        params['token'] = token

        # get lat/long from API
        cnt = 0
        fac_result_all = pd.DataFrame()
        for i in range(0, n_row, BATCH_SIZE):
            fac = facilities_distinct.loc[i:i+BATCH_SIZE-1,]
            fac_json = self.getFacilitiesJSON(fac)
            params['addresses'] = fac_json

            # call API to get lat/long
            status, result_df = self.getFacilitiesLatLong(params=params)

            if status:
                fac_result = fac.merge(result_df, on='facilityId')
                logger.info("Get {} lat/long from API...".format(fac_result.shape[0]))
                fac_result_all = fac_result_all.append(fac_result, ignore_index=True)
            cnt += 1
            # if cnt == 3:
            #     break
        logger.info("Time used for query:")
        logger.info(time.time() - starttime)
        query_mark = time.time()

        fac_result_all_select = fac_result_all[['Address','latitude', 'longitude']]
        facilities_result = facilities.merge(fac_result_all_select, on='Address')
        logger.info(facilities_result.dropna(subset=['Address']).shape)
        logger.info("Time used for merge:")
        logger.info(time.time() - query_mark)

        # write facility df to db
        facilities_result = facilities_result[['facilityId', 'facilityName','externalId','latitude','longitude','geoLocationString']]
        learning_accessor.write_facility_df(facilities_result)
        logger.info("Facility table written to learning db")
        logger.info("Total Time used:")
        logger.info(time.time() - starttime)

    def handleAbbr(self, fac):
        fac['Address'] = fac['Address'].str.upper()
        fac['Address'] = fac.Address.str.replace('\bSTREET\b', 'ST')
        fac['Address'] = fac.Address.str.replace('\bROAD\b', 'RD')
        fac['Address'] = fac.Address.str.replace('\bAVENUE\b', 'AVE')
        fac['Address'] = fac.Address.str.replace('\bTRAIL\b', 'TRL')
        fac['Address'] = fac.Address.str.replace('\bDRIVE\b', 'DR')
        fac['Address'] = fac.Address.str.replace('\bBOULEVARD\b', 'BLVD')
        fac['Address'] = fac.Address.str.replace('\bCENTER\b', 'CTR')
        fac['Address'] = fac.Address.str.replace('\bCIRCLE\b', 'CIR')
        fac['Address'] = fac.Address.str.replace('\bCOURT\b', 'CT')
        fac['Address'] = fac.Address.str.replace('\bEXPRESSWAY\b', 'EXPY')
        fac['Address'] = fac.Address.str.replace('\bHEIGHTS\b', 'HTS')
        fac['Address'] = fac.Address.str.replace('\bHIGHWAY\b', 'HWY')
        fac['Address'] = fac.Address.str.replace('\bISLAND\b', 'IS')
        fac['Address'] = fac.Address.str.replace('\bSUITE\b', 'STE')
        fac['Address'] = fac.Address.str.replace('\bSQUARE\b', 'SQ')

        return fac

    def getAPIToken(self, client_id, client_secret):
        params_dict = dict()
        params_dict['f'] = 'pjson'
        params_dict['client_id'] = client_id
        params_dict['grant_type'] = 'client_credentials'
        params_dict['client_secret'] = client_secret
        params_dict['expiration'] = EXPIRATION
        token = None
        try:
            resp = requests.get(url=TOKEN_URL, headers={'User-Agent': 'Mozilla/5.0'}, params=params_dict)
            token = resp.json()['access_token']
            logger.info("Successfully got token")
        except Exception as e:
            logger.info("Fail to get token: {}".format(e))
        return token

    def getFacilitiesJSON(self, fac):
        fac = fac[['facilityId', 'Address']]
        fac = fac.rename(columns={"Address": "SingleLine", "facilityId": "OBJECTID"})
        fac_list = json.loads(fac.to_json(orient="records"))
        fac_list_att = [{"attributes": f} for f in fac_list]
        fac_list_att_all = {"records": fac_list_att}
        return json.dumps(fac_list_att_all)

    def getFacilitiesLatLong(self, params):
        resp = requests.post(url=FIND_URL, headers={'User-Agent': 'Mozilla/5.0'}, data=params)
        status = resp.status_code
        if status == 200:
            locations = resp.json()['locations']
            locations_list = [{'facilityId': res['attributes']['ResultID'], 'longitude': res['location']['x'],\
                               'latitude': res['location']['y']} for res in locations]
            locations_df = pd.DataFrame(locations_list)
            logger.info("Facility found")
            return [True, locations_df]
        elif status == 404:
            logger.info("Not found")
        elif status == 504:
            logger.info("Gateway time out")
        return [False, None]


def main():
    """
    This is main function
    """
    # Validate the input argument to the script

    # Retrieve the arguments
    input_args_dict = dict(i.split("=") for i in sys.argv[1:])
    try:
        db_host = input_args_dict['dbhost']
        db_user = input_args_dict['dbuser']
        db_password = input_args_dict['dbpassword']
        dse_db_name = input_args_dict['dbname']
        db_port = input_args_dict['port']  # Update to 33066 for testing on Local machine
        # db_port = 33066
        customer_name = input_args_dict['customer']

        # run_date = input_args_dict['rundate']
        learning_home_dir = input_args_dict['homedir']
        cs_db_name = input_args_dict['dbname_cs']
        learning_db_name = input_args_dict['dbname_learning']
    except KeyError as e:
        print("Could not get parameters:{}".format(e))
        return

    database_config = DatabaseConfig.instance()
    database_config.set_config(db_host, db_user, db_password, db_port, dse_db_name, learning_db_name, cs_db_name)

    geocodeGenerator = GeocodeGeneratorDriver(learning_home_dir, customer_name)
    geocodeGenerator.startGeocodeFacility()


if __name__ == "__main__":
    main()




