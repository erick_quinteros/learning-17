##########################################################
#
#
# aktana- update API config for different envs
#
# description: update API config for different envs
#
# created by : learning-dev@aktana.com
#
# created on : 2019-06-05
#
# Copyright AKTANA (c) 2019.
#
#
####################################################################################################################
library(futile.logger)
library(jsonlite)
library(data.table)

# set parameters 
args <- commandArgs(T)

if(length(args)==0){
  print("No arguments supplied.")
  if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
  print("Arguments supplied.")
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
    print(args[[i]]);
  }
}
if (!exists('port')) {
  port <- 0
} else {
  port <- as.numeric(port)
}

# source dbconnection
source(sprintf("%s/common/dbConnection/dbConnection.R", homedir))

con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname_learning, port)
apisecret <- data.table(dbGetQuery(con_l,"SELECT value FROM `sysParameters` WHERE name='apitokenSecret';"))$value[1]

if (!is.null(apisecret)) {
  flog.info("successfully fetch api secret")  
}


# read config
# newAPIConfig <- fromJSON(localAPIConfigFilePath)[[customer]][[envname]]
apiurl = sprintf("https://%sdse%s.aktana.com/%s/learning/api/v1.0/",region,envname,customer)
newAPIConfig <- list("learningApi"=apiurl,"secret"=apisecret, "username"="aktanaadmin", "password"="")
# overwrite API config in common/APICall/envs/env.json
# if (!is.null(newAPIConfig)) {
#   targetAPIConfigFilePath <- sprintf("%s/common/APICall/envs/env.json", homedir)
#   flog.info("change learning API to: %s", newAPIConfig$learningApi)
#   write_json(newAPIConfig, targetAPIConfigFilePath)
# }

# write apiurl to json
write_json(list("apiurl"=apiurl), sprintf("%s/common/APICall/env.json", homedir))

# write s3 bucket path to file for later use
tryCatch({
  # source API call related scripts
  source(sprintf("%s/common/APICall/global.R",homedir))
  # initialize API call params
  init(TRUE, newAPIConfig)
  # Authenticate and get a token
  Authenticate(glblLearningAPIurl)
  # call S3/pathPrefix API
  req <- getAPIJson(glblLearningAPIurl, 'S3/pathPrefix')
  s3BucketPath <- httr::content(req, as = "text")
  flog.info("s3BucketPath for %s/%s is: %s", customer, envname, s3BucketPath)
  # write to json path
  targetS3ConfigFilePath <- sprintf("%s/common/sparkUtils/s3Config.json", homedir)
  write_json(list(s3BucketPath=s3BucketPath, s3BuildDirPath=paste(s3BucketPath,'builds',sep='/')), targetS3ConfigFilePath)
},
error = function(e) {flog.error("Creating s3 path config failed with API Call: %s", e)}
)