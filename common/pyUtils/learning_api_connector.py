import requests
import json

from common.pyUtils.logger import get_module_logger

logger = get_module_logger(__name__)

'''Checks for 200 response, returns None if not 200'''
def handle_response(call_type, url, response):
    if response.status_code == 200:
        logger.info("{call_type} request with url: {url} successful, returning response body".format(call_type=call_type, url=url))
        return response.text
    logger.info("{call_type} request with url: {url} did not return 200 code, returning None".format(call_type=call_type, url=url))
    logger.info(response.text)
    return None


'''Class to handle learning api calls'''
class LearningApiConnector:

    def __init__(self, customer, environment, learning_home_dir):
        self.customer = customer
        self.environment = environment
        self.learning_home_dir = learning_home_dir
        self.learning_api_url = None
        self.learning_api_token = None
        self._fetch_learning_token()

    '''Desearlize the api info json from the learning dir'''
    def _read_api_info_json(self):
        logger.info("Reading API config json")
        try:
            with open(self.learning_home_dir + '/common/sparkUtils/APIConfig.json') as json_file:
                data = json.load(json_file)
                learning_api_info = data[self.customer][self.environment]
                logger.info("Successfully read API config json for customer: {customer} and environment: {environment}".format(customer=self.customer, environment=self.environment))
                return learning_api_info
        except Exception:
            logger.error("Failed reading API config json for customer: {customer} and environment: {environment}".format(customer=self.customer, environment=self.environment))
            raise

    '''Fetch a learning token, save to class'''
    def _fetch_learning_token(self):
        logger.info("Generating an api token")
        learning_api_info = self._read_api_info_json()
        self.learning_api_url = learning_api_info.pop('learningApi') # read api url and drop from dict
        self.learning_api_token = self.make_post_call('Token', learning_api_info, {"Content-Type": "application/json"}) # post call with custom header

    '''Makes a standard header with baearer auth adn app/json type'''
    def _get_standard_header(self):
        return {"Content-Type": "application/json", 'Authorization': 'Bearer {0}'.format(self.learning_api_token)}

    '''Makes a get call with a url suffix and optional custom headers'''
    def make_get_call(self, url_suffix, headers=None):
        url = self.learning_api_url + url_suffix
        if headers is None:  headers = self._get_standard_header()
        response = requests.get(url, headers=headers)
        return handle_response("GET", url, response)

    '''Makes a post call with a url suffix, json body and optional custom headers'''
    def make_post_call(self, url_suffix, json_body, headers=None):
        url = self.learning_api_url + url_suffix
        if headers is None:  headers = self._get_standard_header()
        response = requests.post(url, headers=headers, json=json_body)
        return handle_response("POST", url, response)

    '''Makes a post call with a url suffix, json body and optional custom headers'''
    def make_put_call(self, url_suffix, json_body, headers=None):
        url = self.learning_api_url + url_suffix
        if headers is None:  headers = self._get_standard_header()
        response = requests.post(url, headers=headers, data=json_body)
        return handle_response("PUT", url, response)

    '''Makes a delete call with a url suffix, json body and optional custom headers'''
    def make_delete_call(self, url_suffix, headers=None):
        url = self.learning_api_url + url_suffix
        if headers is None:  headers = self._get_standard_header()
        response = requests.delete(url, headers=headers)
        return handle_response("DELETE", url, response)