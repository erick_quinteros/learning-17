context('testing readV3dbFilter from learningPackage')
print(Sys.time())

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list(pfizerusdev=c('Product','RepTeam','Message','MessageSetMessage','MessageSet','Event','EventType','Interaction','ProductInteractionType','InteractionProduct','InteractionAccount','InteractionProductMessage','AccountProduct','Rep','RepTeamRep','RepAccountAssignment','Account'))
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList)

# loading Learning package to call function for test
library(Learning)

# set required paramter
drv <- dbDriver("MySQL")
con <- dbConnect(drv,user=dbuser,host=dbhost,dbname=dbname,port=port,password=dbpassword)

# call readV3Filter for defined productUID
productUID <- readModuleConfig(homedir, 'common/unitTest','productUID')
dictraw <- readV3dbFilter(con, productUID)
names(dictraw) <- paste(names(dictraw),"_new",sep="")
for(i in 1:length(dictraw))assign(names(dictraw)[i],dictraw[[i]])

# test case
test_that("test have correct length of data for defined productUID", {
  expect_equal(length(dictraw), 8)
  expect_equal(dim(accounts_new),c(310,46))
  expect_equal(dim(interactions_new),c(16592,12))
  expect_equal(dim(messages_new),c(446,7))
  expect_equal(dim(messageSetMessage_new),c(10,2))
  expect_equal(dim(messageSet_new),c(4,2))
  expect_equal(dim(accountProduct_new),c(309,170))
  expect_equal(dim(events_new),c(390,9))
  expect_equal(dim(products_new),c(1,2))
})
rm(dictraw)

test_that("result is the same as saved for defined productUID", {
  # load saved data for comparison
  load(sprintf("%s/common/unitTest/data/from_readV3db_accounts.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_interactions.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messages.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messageSetMessage.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messageSet.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_accountProduct.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_events.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_products.RData", homedir))
  # comapre with new results
  expect_equal(accounts_new,accounts)
  cols <- names(interactions_new)
  interactions$productInteractionTypeName <- as.character(interactions$productInteractionTypeName)
  interactions$productName <- as.character(interactions$productName)
  expect_equal(interactions_new[order(interactions_new$interactionId),],interactions[order(interactions$interactionId),][interactions$productName=="CHANTIX",..cols])
  expect_equal(messages_new,messages)
  expect_equal(messageSetMessage_new,messageSetMessage)
  expect_equal(messageSet_new,messageSet)
  expect_equal(accountProduct_new,accountProduct[productName=="CHANTIX"])
  expect_equal(events_new,events[productName=="CHANTIX"])
  expect_equal(products_new,products[productName=="CHANTIX"])
})

# call readV3dbFilter again for productUID=All and prod=NULL
productUID <- "All"
dictraw <- readV3dbFilter(con,productUID)
names(dictraw) <- paste(names(dictraw),"_new",sep="")
for(i in 1:length(dictraw))assign(names(dictraw)[i],dictraw[[i]])

# test case
test_that("test have correct length of data for productId=All", {
  expect_equal(length(dictraw), 8)
  expect_equal(dim(accounts_new),c(310,46))
  expect_equal(dim(interactions_new),c(47763,12))
  expect_equal(dim(messages_new),c(446,7))
  expect_equal(dim(messageSetMessage_new),c(10,2))
  expect_equal(dim(accountProduct_new),c(560,2))
  expect_equal(dim(events_new),c(733,9))
  expect_equal(dim(products_new),c(4,2))
})
rm(dictraw)

test_that("result is the same as saved for productId=All", {
  # load saved data for comparison
  load(sprintf("%s/common/unitTest/data/from_readV3db_accounts.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_interactions.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messages.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messageSetMessage.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messageSet.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_accountProduct.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_events.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_products.RData", homedir))
  # comapre with new results
  expect_equal(accounts_new,accounts)
  cols <- names(interactions_new)
  interactions$productInteractionTypeName <- as.character(interactions$productInteractionTypeName)
  interactions$productName <- as.character(interactions$productName)
  expect_equal(interactions_new[order(interactionId, accountId),],interactions[order(interactionId, accountId),][,..cols])
  expect_equal(messages_new,messages)
  expect_equal(messageSetMessage_new,messageSetMessage)
  expect_equal(messageSet_new,messageSet)
  expect_equal(accountProduct_new,accountProduct[, c('accountId','productName')])
  expect_equal(events_new[order(repId,accountId,eventDateTimeUTC),],events[order(repId,accountId,eventDateTimeUTC),])
  expect_equal(products_new,products)
})

# call readV3dbFilter again for productUID=All and defined prods
load(sprintf("%s/common/unitTest/data/from_readV3db_accountProduct.RData", homedir))
productUID <- "All"
prods <- colnames(accountProduct)[!colnames(accountProduct) %in% c("accountId","productName")]
dictraw <- readV3dbFilter(con,productUID, prods=prods)
accountProduct_new <- dictraw[['accountProduct']]
test_that("check accountProduct for productId=All with defined prods", {
  expect_equal(dim(accountProduct_new),c(560,170))
  expect_equal(accountProduct_new,accountProduct)
})

# disconnect DB
dbDisconnect(con)
