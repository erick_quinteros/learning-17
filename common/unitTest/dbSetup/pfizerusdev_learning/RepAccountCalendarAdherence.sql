CREATE TABLE `RepAccountCalendarAdherence` (
  `repId` int(11) NOT NULL,
  `accountId` int(11) NOT NULL,
  `completeProb` double NOT NULL,
  `completeConfidence` double NOT NULL,
  `rescheduleProb` double NOT NULL,
  `rescheduleConfidence` double NOT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `runDate` date NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`repId`,`accountId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;