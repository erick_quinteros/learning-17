CREATE TABLE `SimulationAccountSegment` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `segmentUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `accountUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `accountName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`learningBuildUID`,`segmentUID`,`accountUID`),
  KEY `SimulationAccountSegment_idx` (`learningBuildUID`,`accountUID`),
  KEY `SimulationAccountSegment_fk_2` (`learningRunUID`)
  /* CONSTRAINT `SimulationAccountSegment_fk_1` FOREIGN KEY (`learningBuildUID`) REFERENCES `LearningBuild` (`learningBuildUID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SimulationAccountSegment_fk_2` FOREIGN KEY (`learningRunUID`) REFERENCES `LearningRun` (`learningRunUID`) ON DELETE CASCADE ON UPDATE CASCADE */
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
