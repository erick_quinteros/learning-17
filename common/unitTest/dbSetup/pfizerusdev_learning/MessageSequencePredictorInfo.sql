CREATE TABLE `MessageSequencePredictorInfo` (
  `productUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `channelUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `goal` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `predictor` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `predictorValue` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `probability` double DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `explanationFit` double DEFAULT NULL,
  `source` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productUID`,`channelUID`,`goal`,`predictor`,`predictorValue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
