CREATE TABLE `InteractionType` (
  `interactionTypeId` tinyint(4) NOT NULL,
  `interactionTypeName` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`interactionTypeId`),
  UNIQUE KEY `interactionType_uniqueName` (`interactionTypeName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci