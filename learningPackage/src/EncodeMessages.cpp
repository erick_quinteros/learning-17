#include <RcppArmadillo.h>
#include <stack>
// #include <armadillo>
#include <unordered_map>
using namespace Rcpp;

// [[Rcpp::depends(RcppArmadillo)]]

// [[Rcpp::export]]
DataFrame EncodeMessages(DataFrame events, std::string targetName, std::string sendName)
{
  IntegerVector accounts = as<IntegerVector>(events["accountId"]);
  CharacterVector key = as<CharacterVector>(events["key"]);
  // DateVector eventDate = as<DateVector>(events["date"]);
  IntegerVector priorVisit = as<IntegerVector>(events["priorVisit"]);
  IntegerVector measures;
  CharacterVector values;
  IntegerVector accountId;
  IntegerVector recordId;
  int targetLen = targetName.length();
  int sendLen = sendName.length();
  int account=accounts[1];
  int recordNo=1;
  int end=0;
  std::unordered_map<int,int> accountMap;  // save the subscript of the first element of each unique accountId

  // Rcout << "accounts.size" << accounts.size() << "\n";
    
  for(int i = 0; i<accounts.size(); i++){ if(accountMap.count(accounts[i])==0)accountMap.insert(std::make_pair(accounts[i],i)); }

  for(std::unordered_map<int,int>::iterator it = accountMap.begin(); it != accountMap.end(); it++)
    {
      account=it->first;
      recordNo=1;
          // Rcout << "account " << account << "\n";
          // Rcout << "accounts[it->second] " << accounts[it->second] << "\n";
          // Rcout << "subscript " << it->second << "\n";
          // Rcout << "Target Name == " << targetName << "\n";
          // Rcout << key << std::endl;

      std::vector<int> pail;
      int i = it->second;
      for(; (accounts[i]==account) & (i<key.size()); i++)
	{
	  if (targetName.compare(0,targetLen,key[i])==0) { pail.push_back(i); }
	}
      if(pail.size()==0)pail.push_back(i-1);
      
                  // Rcout << "pail size " << pail.size() << "\n";
    
      // now go through the data for each target 
      int start=it->second;
      for(std::vector<int>::iterator it = pail.begin(); it != pail.end(); it++)
	{
	  end = *it;
	  //	  // Rcout << "start = " << start << " end = " << end << "\n";
	  int maxSendNameInd = -1;
	  for(int i = start; i<=end; i++)
	    {
	      measures.push_back(1);
	      values.push_back(key[i]);
	      accountId.push_back(account);
	      recordId.push_back(recordNo);
	      // get whether it has prior call or not
	      // find sendName index
	      if (sendName.compare(0,sendLen,key[i])==0) { maxSendNameInd = i; }
	    }
	  // add prior call if there is prior call for the latest sendName for the record
	  if (maxSendNameInd>=0 && priorVisit[maxSendNameInd]>=0) {
	    measures.push_back(priorVisit[maxSendNameInd]);
	    values.push_back("priorVisit");
	    accountId.push_back(account);
	    recordId.push_back(recordNo);
	  }
	  start=end+1;
	  recordNo++;
	}
    }
    // Rcout << values << std::endl;
    // Rcout << accountId << std::endl;
    // Rcout << recordId << std::endl;
  
  DataFrame myDF = List::create(recordId,accountId,values,measures);
  myDF.attr("names") = CharacterVector::create("recordId","accountId","value","measure");

  return myDF;
}
